# Squad

## Teste para novos programadores backend da Squad

Para instalar as depêndencias do projeto:

```pip install -r requirements.txt```

O teste consiste em criar 3 endpoints relacionados a cidades e estados.

Para conectar com o banco de dados da sua máquina é necessário criar o banco. Comando:

```createdb teste_backend```

E trocar as informações de autenticação.

```
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': "teste_backend",
        'USER': "postgres",
        'PASSWORD': "",
        'HOST': "localhost",
        'PORT': '5432',
    }
}
```

Para popular com dados de teste é necessário rodar o comando:

```python manage.py popular_dados```

O comando só funcionará caso o banco de dados esteja corretamente integrado.

## Testes

Será muito bem vindo a escrita de testes para garantir o funcionamento dos endpoints feitos.