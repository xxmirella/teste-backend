from django.db.models import Model, ForeignKey, PROTECT
from django.db.models.fields import CharField


class Estado(Model):
    nome = CharField(max_length=255)
    sigla = CharField(max_length=2)


class Cidade(Model):
    nome = CharField(max_length=255)
    estado = ForeignKey(
        Estado,
        related_name="cidades",
        on_delete=PROTECT,
    )

