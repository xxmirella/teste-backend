from rest_framework.views import APIView
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response
from .serializers import EstadoSerializer, CidadeSerializer
from .models import Estado, Cidade


class ListarEstadosAPIView(APIView):

    """
    Especificação do endpoint

    [GET /estados/]

    Response 200:
    {
        "estados": [
            {
                "nome": "São Paulo",
                "id": 1,
                "sigla": "SP",
                cidades: [
                    {
                        "nome": "São Paulo",
                        "id": 1,
                    },
                    ...
                ]
            },
            {
                "nome": "Santa Catarina",
                "id": 2,
                "sigla": "SC",
                cidades: [
                    {
                        "nome": "Florianópolis",
                        "id": 2,
                    },
                    ...
                ]
            },
            ...
        ]
    }

    """

    def get(self, request, *args, **kwargs):
        estados = Estado.objects.all()
        serializer = EstadoSerializer(estados, many=True)
        return Response({'Estados': serializer.data}, status=200)


class CidadesDeUmEstadoAPIView(APIView):

    """

    Especificação do endpoint

    [POST /estado/cidades/]

    Request:
    {
        "sigla": "sp"
    }

    Response 200:
    {
        "cidades": [
            {
                "nome": "São Paulo",
                "id": 1
            },
            ...
        ]
    }

    """

    def post(self, request, *args, **kwargs):
        sigla = request.data.get('sigla', [])
        if not sigla:
            return Response(status=404)

        if sigla.islower():
            sigla = sigla.upper()
        cidades = Cidade.objects.filter(estado__sigla=sigla)
        serializer = CidadeSerializer(cidades, many=True)
        return Response({'Cidades': serializer.data}, status=200)


class AutocompleteCidadeAPIView(APIView):

    """

    Especificação do endpoint

    [POST /cidade/autocomplete/]

    Request:
    {
        "text": "S"
    }

    Response 200:
    {
        "cidades": [
            {
                "nome": "São Paulo",
                "id": 1
            },
            {
                "nome": "Sorocaba",
                "id": 1
            },
        ]
    }

    ou

    Request:
    {
        "text": "Lond"
    }

    Response 200:
    {
        "cidades": [
            {
                "nome": "Londrina",
                "id": 1
            },
            ...
        ]
    }

    """

    def post(self, request, *args, **kwargs):
        contem = request.data.get('text', [])
        if not contem:
            return Response(status=404)

        cidades = Cidade.objects.filter(nome__startswith=contem)
        serializer = CidadeSerializer(cidades, many=True)
        return Response({'Cidades': serializer.data}, status=200)
