from rest_framework import serializers
from .models import Estado, Cidade


class CidadeSerializer(serializers.ModelSerializer):

    class Meta:
        model = Cidade
        fields = ['nome', 'id']


class EstadoSerializer(serializers.ModelSerializer):
    cidades = CidadeSerializer(read_only=True, many=True)

    class Meta:
        model = Estado
        fields = ['nome', 'id', 'sigla', 'cidades']
